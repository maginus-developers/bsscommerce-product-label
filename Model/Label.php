<?php
/**
 *
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category  BSS
 * @package   Bss_ProductLabel
 * @author    Extension Team
 * @copyright Copyright (c) 2017-2018 BSS Commerce Co. ( http://bsscommerce.com )
 * @license   http://bsscommerce.com/Bss-Commerce-License.txt
 */

namespace Bss\ProductLabel\Model;

use Magento\Rule\Model\AbstractModel;
use Bss\ProductLabel\Model\ResourceModel\Label as ResourceModelLabel;

/**
 * Class Label
 * @package Bss\ProductLabel\Model
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @codingStandardsIgnoreFile
 */
class Label extends AbstractModel
{
    /**
     * @var array
     */
    protected $productIds = [];

    /**
     * @var \Bss\ProductLabel\Helper\ModelLabel
     */
    protected $helperModel;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json
     */
    protected $json;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $localeDate;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resource;

    /**
     * @var array
     */
    private $websitesMap = [];

    /**
     * @var array
     */
    private $newProductLabelData = [];

    /**
     * Insert records batch size
     * @var int
     */
    private $batchSize = 1000;

    /**
     * Label constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Bss\ProductLabel\Helper\ModelLabel $helperModel
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $abstractResource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Bss\ProductLabel\Helper\ModelLabel $helperModel,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Model\ResourceModel\AbstractResource $abstractResource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        $this->helperModel = $helperModel;
        $this->localeDate = $localeDate;
        $this->resource = $resource;
        parent::__construct($context, $registry, $formFactory, $localeDate, $abstractResource, $resourceCollection, $data);
    }

    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init(ResourceModelLabel::class);
    }

    /**
     * Get rule condition product combine model instance
     *
     * @return \Magento\CatalogRule\Model\Rule\Condition\Product
     */
    public function getActionsInstance()
    {
        return $this->helperModel->getConditionProduct()->create();
    }

    /**
     * Get rule condition combine model instance
     *
     * @return \Magento\CatalogRule\Model\Rule\Condition\Combine
     */
    public function getConditionsInstance()
    {
        return $this->helperModel->getCondCombineFactory()->create();
    }

    /**
     * Full Reindex
     */
    public function doExecuteFull()
    {
        $connection = $this->resource->getConnection();
        $labelTable = $this->resource->getTableName('bss_productlabel_label');

        $sql = $connection->select()->from(
            $labelTable,
            ['*']
        );

        $this->deleteAllIndexedData();

        $query = $connection->query($sql);
        while ($row = $query->fetch()) {
            $this->_resetConditions();
            $this->setData($row);
            if ($this->isApplyLabel()) {
                $this->filterProducts();
            }
            $this->setData([]);
        }

        $labelFlatTable = $this->resource->getTableName('bss_productlabel_flat');
        if (!empty($this->newProductLabelData) &&
            count($this->newProductLabelData) < $this->batchSize) {
            $connection->insertMultiple($labelFlatTable, $this->newProductLabelData);
        }
    }

    /**
     * Filter Product by conditions
     */
    public function filterProducts()
    {
        try {
            $productCollection = $this->helperModel->getProductCollectionFactory()->create();
            $this->setCollectedAttributes([]);
            $this->getConditions()->collectValidatedAttributes($productCollection);
            $this->helperModel->getIterator()->walk(
                $productCollection->getSelect(),
                [[$this, 'callbackValidateProduct']],
                [
                    'attributes' => $this->getCollectedAttributes(),
                    'product' => $this->helperModel->getProductFactory()->create()
                ]
            );
        } catch (\Exception $exception) {
            $this->_logger->critical($exception);
        }
    }

    /**
     * Callback function for product matching
     *
     * @param array $args
     * @return void
     */
    public function callbackValidateProduct($args)
    {
        $labelFlatTable = $this->resource->getTableName('bss_productlabel_flat');
        $product = clone $args['product'];
        $product->setData($args['row']);
        $websites = $this->getWebsitesMap();
        foreach ($websites as $defaultStoreId) {
            $product->setStoreId($defaultStoreId);
            if ($this->getConditions()->validate($product)) {
                $this->newProductLabelData[] = [
                    'label_id' => $this->getId(),
                    'product_id' => $product->getId(),
                    'store_views' => $this->getStoreViews(),
                    'image' => $this->getImage(),
                    'image_data' => $this->getImageData(),
                    'customer_groups' => $this->getCustomerGroups(),
                    'valid_start_date' => $this->getValidStartDate(),
                    'valid_end_date' => $this->getValidEndDate(),
                    'priority' => $this->getPriority()
                ];

                if (count($this->newProductLabelData) == $this->batchSize) {
                    $this->getConnection()->insertMultiple($labelFlatTable, $this->newProductLabelData);
                    $this->newProductLabelData = [];
                }
            }
        }
    }

    /**
     * Prepare website map
     *
     * @return array
     */
    protected function getWebsitesMap()
    {
        if (empty($this->websitesMap)) {
            $websites = $this->helperModel->getStoreManager()->getWebsites();
            foreach ($websites as $website) {
                // Continue if website has no store to be able to create catalog rule for website without store
                if ($website->getDefaultStore() === null) {
                    continue;
                }
                $this->websitesMap[$website->getId()] = $website->getDefaultStore()->getId();
            }
        }

        return $this->websitesMap;
    }

    /**
     * Clean flat table
     *
     * @return $this
     */
    protected function deleteAllIndexedData()
    {
        $this->getConnection()->delete($this->resource->getTableName('bss_productlabel_flat'));
        return $this;
    }

    /**
     * @param int $labelId
     */
    protected function deleteIndexedLabel($labelId)
    {
        $connection = $this->getConnection();
        $labelFlatTable = $this->resource->getTableName('bss_productlabel_flat');

        $query = $connection->deleteFromSelect(
            $connection->select()
                ->from($labelFlatTable, 'label_id')
                ->where('label_id = ?', $labelId),
            $labelFlatTable
        );
        $connection->query($query);
    }

    /**
     * @return \Magento\Framework\DB\Adapter\AdapterInterface
     */
    private function getConnection()
    {
        return $this->resource->getConnection();
    }

    /**
     * Check label is allow to apply
     * @return bool
     */
    public function isApplyLabel()
    {
        if (!$this->getActive() ||
            empty($this->getImage()) ||
            empty($this->getImageData()) ||
            !$this->checkValidEndDate($this->getValidEndDate()) ||
            $this->getData('apply_outofstock_product')) {
            return false;
        }

        return true;
    }

    /**
     * Check label valid end date
     * @param string $endDate
     * @return bool
     */
    private function checkValidEndDate($endDate)
    {
        if (!empty($endDate)) {
            $dateTimeZone = $this->localeDate->date()->format('Y-m-d H:i:s');
            $currentTime = strtotime($dateTimeZone);
            //check $endDate > $current_time
            if ($currentTime > strtotime($endDate)) {
                return false;
            }
        };

        return true;
    }

    /**
     * @return AbstractModel
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeDelete()
    {
        $this->deleteIndexedLabel($this->getId());
        return parent::beforeDelete();
    }
}
